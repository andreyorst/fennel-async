## async.fnl v0.0.4 (???)

- Change agents so each has its own internal queue.
- Advance threads when interacting with agents.
- Allow channels to be closed via the `close!` function.
- Add `pipeline` and `pipeline*` functions.

## async.fnl v0.0.3 (2022-09-01)

- Add `take-all` function.
- Add TCP REPL support for remote code evaluation.
- Make server's handler function optionally accept a client as a second argument.
- Add `put-all` function to put a table of values onto a channel at once.
- Add `zip*` function to wait for a table of promises.
- Make either `luasocket` or `posix` a mandatory requirement.
- Add asynchronous `map` function.
- Fix bug when awaiting the promise could oversleep by a large amount.
- Bump minimum required Fennel version to 1.2.0.

## async.fnl v0.0.2 (2022-06-13)

- Add basic "non-blocking" disk IO module.
- Add (experimental) asynchronous TCP module.

## async.fnl v0.0.1 (2022-01-09)

Initial release of the async.fnl library.

<!--  LocalWords:  destructuring metamethod multi
 -->
