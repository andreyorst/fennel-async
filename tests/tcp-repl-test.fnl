(require-macros :fennel-test)
(local async (require :async))
(local tcp async.tcp)

(local lprint _G.print)

(fn take-all [c]
  (let [timeout (setmetatable {} {:__name "timeout"})]
    (async.sleep 100)
    ((fn loop [res]
       (match res
         timeout (tostring res)
         _ (match (async.take c 100 timeout)
             timeout res
             data (loop (.. res data)))))
     (async.take c 10000 timeout))))

(when tcp
  (deftest repl-test
    (testing "running a tcp repl"
      (let [s (tcp.start-repl {:host "localhost"})
            port (tcp.get-port s)
            c (tcp.connect {:host "localhost" :port port})]
        (assert-is (async.try-put c "(+ 1 2 3)"))
        (assert-eq 6 (tonumber (take-all c)))
        (tcp.stop-server s))))
  (deftest error-test
    (testing "error in repl"
      (let [s (tcp.start-repl {:host "localhost"})
            port (tcp.get-port s)
            c (tcp.connect {:host "localhost" :port port})]
        (assert-is (async.try-put c "(+ 1 2"))
        (assert-is (string.match (take-all c) "Parse error"))
        (tcp.stop-server s))
      (let [s (tcp.start-repl {:host "localhost"})
            port (tcp.get-port s)
            c (tcp.connect {:host "localhost" :port port})]
        (assert-is (async.try-put c "x"))
        (assert-is (string.match (take-all c) "Compile error"))
        (tcp.stop-server s)))))
