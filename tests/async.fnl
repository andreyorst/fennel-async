(require-macros :fennel-test)
(local {: run
        : park
        : await
        : sleep
        : promise
        : error!
        : alt
        : deliver
        : zip
        : zip*
        : agent
        : send
        : agent-error
        : restart-agent
        : chan
        : put
        : put-all
        : take
        : take-all
        : buffer
        : dropping-buffer
        : sliding-buffer
        &as async}
  (require :async))

(use-fixtures :each (fn [t] (t) (run)))

(deftest promise-test
  (testing "promise internal structure"
    (let [p (promise)]
      (assert-eq p {:val nil :ready false})
      (deliver p 1)
      (assert-eq p {:val 1 :ready true})))
  (testing "dereferencing"
    (let [p (promise)]
      (assert-eq (await p 0 :not-set) :not-set)
      (assert-eq (p:deref 0 :not-set) :not-set)
      (deliver p 2)
      (assert-eq (await p 0 :not-set) 2)
      (assert-eq (p:deref 0 :not-set) 2)
      (assert-eq (await p) 2)
      (assert-eq (p:deref) 2)))
  (testing "promise delivered only once"
    (let [p (promise)]
      (deliver p 3)
      (assert-eq (await p) 3)
      (deliver p 4)
      (assert-eq (await p) 3))))

(deftest agent-test
  (testing "agent internal structure"
    (let [a (agent 1)]
      (assert-eq a {:val 1 :state :running :queue {} :error nil})))
  (testing "dereferencing"
    (let [a (agent 2)]
      (assert-eq (await a) 2)
      (assert-eq (a:deref) 2)))
  (testing "changing agent with send"
    (let [a (agent 3)]
      (assert-eq (await a) 3)
      (send a (fn [val] (+ val 1)))
      (assert-eq (await a) 4)))
  (testing "agent error"
    (let [a (agent 5)
          e {}]
      (assert-eq (await a) 5)
      (send a (fn [val] (error e)))
      (assert-eq (agent-error a) e)
      (assert-eq (match (pcall await a) (false _) _) e)
      (restart-agent a 42)
      (assert-eq (await a) 42))))

(deftest buffer-test
  (testing "buffer internal structure"
    (let [b (buffer)
          bb (buffer 10)
          db (dropping-buffer 10)
          sb (sliding-buffer 10)]
      (assert-eq b {:size math.huge})
      (assert-eq bb {:size 10})
      (assert-eq db {:size 10})
      (assert-eq sb {:size 10})))
  (testing "error handling"
    (assert-not (pcall buffer "10"))
    (assert-not (pcall dropping-buffer))
    (assert-not (pcall dropping-buffer "10"))
    (assert-not (pcall sliding-buffer))
    (assert-not (pcall sliding-buffer "10"))))

(deftest chan-test
  (testing "channel internal structure"
    (let [c1 (chan)
          c2 (chan 10)
          c3 (chan (buffer))
          c4 (chan (buffer 10))
          c5 (chan (dropping-buffer 10))
          xform #$
          c6 (chan nil xform)
          c7 (chan 10 xform)
          c8 (chan (dropping-buffer 10) xform)
          c9 (chan (sliding-buffer 10))
          c10 (chan (sliding-buffer 10) xform)]
      (assert-eq c1 {:buffer {:size math.huge} :xform nil :closed? false})
      (assert-eq c2 {:buffer {:size 10} :xform nil  :closed? false})
      (assert-eq c1 c3)
      (assert-eq c2 c5)
      (assert-eq c6 {:buffer {:size math.huge} : xform :closed? false})
      (assert-eq c7 {:buffer {:size 10} : xform :closed? false})
      (assert-eq c7 c8 c10)
      (assert-eq c9 {:buffer {:size 10} :xform nil :closed? false})))
  (testing "put all, take all"
    (let [c (chan)]
      (put-all c ["a" "b" "c"])
      (assert-eq ["a" "b" "c"] (take-all c 100))))
  (testing "put and take"
    (let [c1 (chan)
          c2 (chan 10)]
      (assert-is (put c1 "a"))
      (assert-eq (take c1) "a")
      (assert-is (put c2 "b"))
      (assert-eq (take c2) "b")))
  (testing "put with xform"
    (let [c1 (chan nil #(if (= 0 (% $ 2)) $))]
      (assert-is (put c1 1000))
      (assert-is (put c1 1001))
      (assert-eq (take c1) 1000)
      (assert-eq (take c1 0 :no-val) :no-val)))
  (testing "dropping put"
    (let [c (chan (dropping-buffer 1))]
      (put c "c")
      (assert-eq c {:buffer {1 "c" :size 1} :closed? false})
      (put c "d")
      (assert-eq c {:buffer {1 "c" :size 1} :closed? false})
      (assert-eq (take c) "c")))
  (testing "sliding put"
    (let [c (chan (sliding-buffer 2))]
      (put c "c")
      (assert-eq c {:buffer {1 "c" :size 2} :closed? false})
      (put c "d")
      (assert-eq c {:buffer {1 "c" 2 "d" :size 2} :closed? false})
      (put c "e")
      (assert-eq c {:buffer {1 "d" 2 "e" :size 2} :closed? false})
      (assert-eq (take c) "d")
      (assert-eq (take c) "e")))
  (testing "blocking put"
    (let [c (chan 1)]
      (put c "e")
      (assert-eq c {:buffer {1 "e" :size 1} :closed? false})
      (async #(put c "f"))
      (assert-eq c {:buffer {1 "e" :size 1} :closed? false})
      (assert-eq (take c) "e")
      (assert-eq c {:buffer {1 "f" :size 1} :closed? false})
      (assert-eq (take c) "f")))
  (testing "blocking take"
    (let [c (chan)]
      (async #(do (sleep 100) (put c 42)))
      (assert-eq (take c 10) nil)
      (assert-eq (take c) 42)))
  (testing "asynchronous take"
    (let [c (chan)]
      (var res nil)
      (async #(set res (take c)))
      (assert-eq res nil)
      (run :once)
      (put c 42)
      (assert-eq res 42)))
  (testing "take with timeout"
    (let [c (chan 1)]
      (assert-eq :no-value (take c 10 :no-value))
      (put c "g")
      (assert-eq "g" (take c 0 :no-value))))
  (testing "take-all with timeout"
    (let [c (chan)]
      (for [i 1 4] (put c i))
      (assert-eq [1 2 3 4] (take-all c 100))))
  (testing "incorrect size type"
    (assert-not (pcall chan "1"))))

(deftest await-test
  (testing "await promise"
    (let [p (promise)]
      (var res nil)
      (async #(set res (await p)))
      (assert-eq res nil)
      (deliver p 10)
      (assert-eq res 10)))
  (testing "await promise with timeout"
    (let [p (promise)]
      (var res nil)
      (async #(set res (await p 100 :not-delivered)))
      (async.run)
      (assert-eq res :not-delivered)
      (deliver p 20)
      (assert-eq res :not-delivered)
      (async #(set res (await p 100 :not-delivered)))
      (assert-eq res 20)))
  (testing "await promise with timeout synchronously"
    (let [p (promise)]
      (var res nil)
      (async #(do (sleep 100) (deliver p 2000)))
      (assert-eq nil (await p 10))
      (assert-eq 2000 (await p))))
  (testing "await multiple promises"
    (let [p1 (promise)
          p2 (promise)]
      (async #(deliver p2 (+ 1 (await p1))))
      (async #(do (park) (deliver p1 30)))
      (assert-eq {1 30 2 31 :n 2} (zip p1 p2)))
    (let [p1 (promise)
          p2 (promise)]
      (async #(deliver p2 (+ 1 (await p1))))
      (async #(do (park) (deliver p1 30)))
      (assert-eq {1 30 2 31 :n 2} (zip* [p1 p2])))))

(deftest cancel-test
  (testing "canceling a task before it's done"
    (let [res []
          p (async (fn [] (park) (tset res 1 42)))]
      (assert-eq res [])
      (async.cancel p)
      (assert-eq res [])
      (async.await p)
      (assert-eq res [])))
  (testing "canceling a task after it's done"
    (let [res []
          p (async (fn [] (park) (tset res 1 42)))]
      (async.await p)
      (assert-eq res [42])
      (async.cancel p)
      (assert-eq res [42])))
  (testing "canceling a task after multiple times"
    (let [res []
          p (async (fn [] (park) (tset res 1 42)))]
      (async.cancel p)
      (async.cancel p)
      (assert-eq res [])
      (async.await p)
      (assert-eq res []))))

(deftest park-test
  (testing "parking main thread does nothing"
    (assert-eq nil (park)))
  (testing "parking queued task prevents it from running"
    (var res nil)
    (async #(do (park) (set res 100)))
    (assert-eq res nil)
    (run)
    (assert-eq res 100)))

(deftest sleep-test
  (testing "all tasks sleep"
    (var res 0)
    (async #(do (sleep 100) (set res (+ res 2))))
    (async #(do (park) (park) (set res (+ res 3))))
    (async #(do (sleep 100) (set res (+ res 4))))
    (run)
    (assert-eq res 9)))

(deftest error-test
  (testing "error in thread"
    (let [e {}]
      (assert-eq e (match (pcall await (async #(error e))) (false _) _))))
  (testing "error in promise"
    (let [p (promise)
          e {}]
      (error! p e)
      (assert-eq e (match (pcall await p) (false _) _))))
  (testing "error in delivered promise"
    (let [p (promise)
          e {}]
      (deliver p 322)
      (error! p e)
      (assert-eq 322 (match (pcall await p) (true v) v))))
  (testing "unsupported mode"
    (assert-not (pcall run :nope)))
  (testing "unsupported reference"
    (assert-not (pcall await {}))))

(deftest module-callable-test
  (testing "module can be called as function"
    (assert-is (async #nil))))

(deftest alt-test
  (testing "alt returns a random choice"
    (let [res (alt (async #42) (async #43))]
      (assert-is (or (= res 42) (= res 43)))))
  (testing "shortest task always wins"
    (let [res (alt (async #(do (park) (park) (park) 44)) (async #(do (park) 45)))]
      (assert-eq 45 res)))
  (testing "alt works asynchronously"
    (let [p1 (promise)
          p2 (promise)]
      (var res nil)
      (async #(set res (alt p1 p2)))
      (assert-eq res nil)
      (deliver p1 46)
      (assert-eq 46 res))))

(deftest pipeline-test
  (testing "pipeline* doesn't spawn more threads than necessary"
    (let [counter {:current 0
                   :max 0}
          inc #(let [{: current : max} counter
                     new (+ current 1)]
                 (tset counter :current new)
                 (when (> new max)
                   (tset counter :max new)))
          dec #(let [{: current : max} counter
                     new (- current 1)]
                 (tset counter :current new)
                 (when (< new 0)
                   (error "what a horrible night to have a curse!")))
          f (fn [_ c]
              (inc)
              (async.sleep 300)
              (async.put c counter.current)
              (async.close! c)
              (dec))]
      (assert-eq [5 5 5 5 5 5 4 3 2 1]
                 (async.take-all
                  (async.pipeline*
                   5
                   (async.chan)
                   f
                   (doto (async.chan)
                     (async.put-all (fcollect [i 1 10] i))))))
      (assert-eq counter.current 0)
      (assert-eq counter.max 5)))
  (testing "pipeline* doesn't spawn more threads than necessary"
    (let [counter {:current 0
                   :max 0}
          inc #(let [{: current : max} counter
                     new (+ current 1)]
                 (tset counter :current new)
                 (when (> new max)
                   (tset counter :max new)))
          dec #(let [{: current : max} counter
                     new (- current 1)]
                 (tset counter :current new)
                 (when (< new 0)
                   (error "what a horrible night to have a curse!")))
          f (fn [_ c]
              (async #(do (inc)
                          (async.sleep 300)
                          (async.put c counter.current)
                          (async.close! c)
                          (dec))))]
      (async.take-all
       (async.pipeline
        5
        (async.chan)
        f
        (doto (async.chan)
          (async.put-all (fcollect [i 1 10] i)))))
      (assert-eq counter.current 0)
      (assert-eq counter.max 5)))
  (testing "pipeline has results in order of inputs"
    (assert-eq
     (async.take-all
      (async.pipeline
       3
       (async.chan)
       (fn [v c]
         (async #(do
                   (async.sleep (+ 300 (math.random 300)))
                   (async.put c v)
                   (async.close! c))))
       (doto (async.chan)
         (async.put-all (fcollect [i 1 10] i)))))
     (fcollect [i 1 10] i)))
  (testing "pipeline* has results in order of completion"
    (assert-eq
     (async.take-all
      (async.pipeline*
       10
       (async.chan)
       (fn [v c]
         (async #(do
                   (async.sleep (* (- 10 v) 200))
                   (async.put c v)
                   (async.close! c))))
       (doto (async.chan)
         (async.put-all (fcollect [i 1 10] i)))))
     (fcollect [i 10 1 -1] i))))
